# Gr4b_Go8loX

The purpose of making this application is to speedily post an article with the theme of **downloading a printer driver.**
so it can be categorized this application is an `SEO tools`.
for a while this application only supports providers from:

| Name |
|--|
| Epson |
| Canon|

but you can develop it further.
all the examples I have made in the code are as easy as possible.

in this application there are several helper functions:

> NewtonsoftJson, RegEx, HttpGET, HttpPost (with cookies),
> CriptoEncryption, this File (config), stringBuilder, UnicodeToAscii

# Screenshoot
![enter image description here](https://image.ibb.co/cGmJKf/Screenshot-22.png)