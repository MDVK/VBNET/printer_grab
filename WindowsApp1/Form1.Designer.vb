﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.tbUrl = New System.Windows.Forms.TextBox()
        Me.btnGet = New System.Windows.Forms.Button()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbCustom = New System.Windows.Forms.CheckBox()
        Me.btnCopy = New System.Windows.Forms.Button()
        Me.btnOpen = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblModelCode = New System.Windows.Forms.Label()
        Me.rtbFinalOutput = New System.Windows.Forms.RichTextBox()
        Me.pb1 = New System.Windows.Forms.ProgressBar()
        Me.lblCRecomd = New System.Windows.Forms.Label()
        Me.lblCDriver = New System.Windows.Forms.Label()
        Me.lblCSoftware = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txExpression = New System.Windows.Forms.RichTextBox()
        Me.btnTest = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.lblCSdk = New System.Windows.Forms.Label()
        Me.lblCFirmware = New System.Windows.Forms.Label()
        Me.lblCUtil = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.RichTextBox3 = New System.Windows.Forms.RichTextBox()
        Me.RichTextBox4 = New System.Windows.Forms.RichTextBox()
        Me.bg1 = New System.ComponentModel.BackgroundWorker()
        Me.rtbCustom = New System.Windows.Forms.RichTextBox()
        Me.cbSelect = New System.Windows.Forms.ComboBox()
        Me.bg2 = New System.ComponentModel.BackgroundWorker()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.bg3 = New System.ComponentModel.BackgroundWorker()
        Me.cbOnTop = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbFiltering = New System.Windows.Forms.ComboBox()
        Me.ttt = New System.Windows.Forms.ToolTip(Me.components)
        Me.cbBlockqoute = New System.Windows.Forms.CheckBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbUrl
        '
        Me.tbUrl.Location = New System.Drawing.Point(140, 11)
        Me.tbUrl.Name = "tbUrl"
        Me.tbUrl.Size = New System.Drawing.Size(288, 22)
        Me.tbUrl.TabIndex = 0
        Me.tbUrl.Text = "https://epson.com/Support/Printers/All-In-Ones/ET-Series/Epson-ET-2650/s/SPT_C11C" &
    "F47201"
        Me.ttt.SetToolTip(Me.tbUrl, "Insert URL on here .. ")
        '
        'btnGet
        '
        Me.btnGet.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGet.Location = New System.Drawing.Point(434, 8)
        Me.btnGet.Name = "btnGet"
        Me.btnGet.Size = New System.Drawing.Size(54, 54)
        Me.btnGet.TabIndex = 1
        Me.btnGet.Text = "GET"
        Me.btnGet.UseVisualStyleBackColor = True
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Location = New System.Drawing.Point(518, 25)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(92, 52)
        Me.RichTextBox1.TabIndex = 2
        Me.RichTextBox1.Text = "" & Global.Microsoft.VisualBasic.ChrW(10)
        Me.RichTextBox1.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(104, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 14)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "URL"
        '
        'cbCustom
        '
        Me.cbCustom.AutoSize = True
        Me.cbCustom.Checked = True
        Me.cbCustom.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbCustom.Location = New System.Drawing.Point(12, 44)
        Me.cbCustom.Name = "cbCustom"
        Me.cbCustom.Size = New System.Drawing.Size(72, 18)
        Me.cbCustom.TabIndex = 5
        Me.cbCustom.Text = "Custom?"
        Me.ttt.SetToolTip(Me.cbCustom, "Checked to Add Additional HTML in End of Result")
        Me.cbCustom.UseVisualStyleBackColor = True
        '
        'btnCopy
        '
        Me.btnCopy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCopy.Location = New System.Drawing.Point(415, 236)
        Me.btnCopy.Name = "btnCopy"
        Me.btnCopy.Size = New System.Drawing.Size(73, 64)
        Me.btnCopy.TabIndex = 6
        Me.btnCopy.Text = "COPY"
        Me.ttt.SetToolTip(Me.btnCopy, "Copy result to Clipboard")
        Me.btnCopy.UseVisualStyleBackColor = True
        '
        'btnOpen
        '
        Me.btnOpen.Location = New System.Drawing.Point(336, 236)
        Me.btnOpen.Name = "btnOpen"
        Me.btnOpen.Size = New System.Drawing.Size(73, 64)
        Me.btnOpen.TabIndex = 7
        Me.btnOpen.Text = "OPEN IN BROWSER"
        Me.ttt.SetToolTip(Me.btnOpen, "Views Result with Your Default Browser")
        Me.btnOpen.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(522, 279)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(81, 14)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Model Code : "
        Me.Label3.Visible = False
        '
        'lblModelCode
        '
        Me.lblModelCode.AutoSize = True
        Me.lblModelCode.Location = New System.Drawing.Point(596, 279)
        Me.lblModelCode.Name = "lblModelCode"
        Me.lblModelCode.Size = New System.Drawing.Size(43, 14)
        Me.lblModelCode.TabIndex = 10
        Me.lblModelCode.Text = "000000"
        Me.ttt.SetToolTip(Me.lblModelCode, "Model Code of Canon")
        Me.lblModelCode.Visible = False
        '
        'rtbFinalOutput
        '
        Me.rtbFinalOutput.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.rtbFinalOutput.Location = New System.Drawing.Point(252, 68)
        Me.rtbFinalOutput.Name = "rtbFinalOutput"
        Me.rtbFinalOutput.ReadOnly = True
        Me.rtbFinalOutput.Size = New System.Drawing.Size(239, 159)
        Me.rtbFinalOutput.TabIndex = 12
        Me.rtbFinalOutput.Text = ""
        '
        'pb1
        '
        Me.pb1.Location = New System.Drawing.Point(156, 306)
        Me.pb1.Name = "pb1"
        Me.pb1.Size = New System.Drawing.Size(329, 17)
        Me.pb1.TabIndex = 13
        '
        'lblCRecomd
        '
        Me.lblCRecomd.AutoSize = True
        Me.lblCRecomd.Location = New System.Drawing.Point(761, 190)
        Me.lblCRecomd.Name = "lblCRecomd"
        Me.lblCRecomd.Size = New System.Drawing.Size(68, 14)
        Me.lblCRecomd.TabIndex = 14
        Me.lblCRecomd.Text = "Recomd = 0"
        Me.lblCRecomd.Visible = False
        '
        'lblCDriver
        '
        Me.lblCDriver.AutoSize = True
        Me.lblCDriver.Location = New System.Drawing.Point(761, 278)
        Me.lblCDriver.Name = "lblCDriver"
        Me.lblCDriver.Size = New System.Drawing.Size(57, 14)
        Me.lblCDriver.TabIndex = 15
        Me.lblCDriver.Text = "Driver = 0"
        Me.lblCDriver.Visible = False
        '
        'lblCSoftware
        '
        Me.lblCSoftware.AutoSize = True
        Me.lblCSoftware.Location = New System.Drawing.Point(761, 209)
        Me.lblCSoftware.Name = "lblCSoftware"
        Me.lblCSoftware.Size = New System.Drawing.Size(72, 14)
        Me.lblCSoftware.TabIndex = 16
        Me.lblCSoftware.Text = "Software = 0"
        Me.lblCSoftware.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txExpression)
        Me.GroupBox1.Controls.Add(Me.btnTest)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 236)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(318, 64)
        Me.GroupBox1.TabIndex = 18
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Option(s)"
        '
        'txExpression
        '
        Me.txExpression.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txExpression.Location = New System.Drawing.Point(9, 34)
        Me.txExpression.Multiline = False
        Me.txExpression.Name = "txExpression"
        Me.txExpression.Size = New System.Drawing.Size(186, 22)
        Me.txExpression.TabIndex = 34
        Me.txExpression.Text = "Install(.*)Driver by"
        Me.ttt.SetToolTip(Me.txExpression, "Field ur RegEx Expression to match some text and make auto replace to this result" &
        " .")
        '
        'btnTest
        '
        Me.btnTest.Location = New System.Drawing.Point(201, 15)
        Me.btnTest.Name = "btnTest"
        Me.btnTest.Size = New System.Drawing.Size(111, 42)
        Me.btnTest.TabIndex = 33
        Me.btnTest.Text = "Test RegEx"
        Me.btnTest.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 17)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(106, 14)
        Me.Label4.TabIndex = 31
        Me.Label4.Text = "RegEx Expression :"
        '
        'Button7
        '
        Me.Button7.Enabled = False
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.Location = New System.Drawing.Point(525, 236)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(86, 25)
        Me.Button7.TabIndex = 2
        Me.Button7.Text = "Spinner Tools"
        Me.Button7.UseVisualStyleBackColor = True
        Me.Button7.Visible = False
        '
        'Button6
        '
        Me.Button6.Enabled = False
        Me.Button6.Location = New System.Drawing.Point(518, 206)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(93, 25)
        Me.Button6.TabIndex = 1
        Me.Button6.Text = "Edit Template"
        Me.Button6.UseVisualStyleBackColor = True
        Me.Button6.Visible = False
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(520, 174)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(100, 25)
        Me.Button5.TabIndex = 0
        Me.Button5.Text = "Open Response"
        Me.Button5.UseVisualStyleBackColor = True
        Me.Button5.Visible = False
        '
        'lblCSdk
        '
        Me.lblCSdk.AutoSize = True
        Me.lblCSdk.Location = New System.Drawing.Point(761, 252)
        Me.lblCSdk.Name = "lblCSdk"
        Me.lblCSdk.Size = New System.Drawing.Size(44, 14)
        Me.lblCSdk.TabIndex = 19
        Me.lblCSdk.Text = "Sdk = 0"
        Me.lblCSdk.Visible = False
        '
        'lblCFirmware
        '
        Me.lblCFirmware.AutoSize = True
        Me.lblCFirmware.Location = New System.Drawing.Point(761, 233)
        Me.lblCFirmware.Name = "lblCFirmware"
        Me.lblCFirmware.Size = New System.Drawing.Size(76, 14)
        Me.lblCFirmware.TabIndex = 18
        Me.lblCFirmware.Text = "Firmware = 0"
        Me.lblCFirmware.Visible = False
        '
        'lblCUtil
        '
        Me.lblCUtil.AutoSize = True
        Me.lblCUtil.Location = New System.Drawing.Point(761, 297)
        Me.lblCUtil.Name = "lblCUtil"
        Me.lblCUtil.Size = New System.Drawing.Size(68, 14)
        Me.lblCUtil.TabIndex = 17
        Me.lblCUtil.Text = "Utilities = 0"
        Me.lblCUtil.Visible = False
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(616, 25)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(82, 54)
        Me.Button4.TabIndex = 19
        Me.Button4.Text = "TESTER"
        Me.Button4.UseVisualStyleBackColor = True
        Me.Button4.Visible = False
        '
        'RichTextBox3
        '
        Me.RichTextBox3.Location = New System.Drawing.Point(16, 28)
        Me.RichTextBox3.Name = "RichTextBox3"
        Me.RichTextBox3.Size = New System.Drawing.Size(58, 58)
        Me.RichTextBox3.TabIndex = 20
        Me.RichTextBox3.Text = ""
        '
        'RichTextBox4
        '
        Me.RichTextBox4.Location = New System.Drawing.Point(108, 29)
        Me.RichTextBox4.Name = "RichTextBox4"
        Me.RichTextBox4.Size = New System.Drawing.Size(58, 58)
        Me.RichTextBox4.TabIndex = 21
        Me.RichTextBox4.Text = ""
        '
        'bg1
        '
        '
        'rtbCustom
        '
        Me.rtbCustom.BackColor = System.Drawing.Color.LightYellow
        Me.rtbCustom.Location = New System.Drawing.Point(12, 68)
        Me.rtbCustom.Name = "rtbCustom"
        Me.rtbCustom.Size = New System.Drawing.Size(239, 159)
        Me.rtbCustom.TabIndex = 22
        Me.rtbCustom.Text = resources.GetString("rtbCustom.Text")
        '
        'cbSelect
        '
        Me.cbSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbSelect.FormattingEnabled = True
        Me.cbSelect.Items.AddRange(New Object() {"epson", "Canon", "Hp"})
        Me.cbSelect.Location = New System.Drawing.Point(12, 11)
        Me.cbSelect.Name = "cbSelect"
        Me.cbSelect.Size = New System.Drawing.Size(86, 22)
        Me.cbSelect.TabIndex = 23
        Me.ttt.SetToolTip(Me.cbSelect, "Select Provider")
        '
        'bg2
        '
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.RichTextBox3)
        Me.GroupBox2.Controls.Add(Me.RichTextBox4)
        Me.GroupBox2.Location = New System.Drawing.Point(850, 215)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(200, 108)
        Me.GroupBox2.TabIndex = 24
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Cannon"
        Me.GroupBox2.Visible = False
        '
        'Timer1
        '
        '
        'Timer2
        '
        Me.Timer2.Interval = 250
        '
        'bg3
        '
        '
        'cbOnTop
        '
        Me.cbOnTop.AutoSize = True
        Me.cbOnTop.Location = New System.Drawing.Point(12, 306)
        Me.cbOnTop.Name = "cbOnTop"
        Me.cbOnTop.Size = New System.Drawing.Size(65, 18)
        Me.cbOnTop.TabIndex = 25
        Me.cbOnTop.Text = "onTop?"
        Me.ttt.SetToolTip(Me.cbOnTop, "Checked to Make Application Always on Top.")
        Me.cbOnTop.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(249, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(27, 14)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "OS :"
        '
        'cbFiltering
        '
        Me.cbFiltering.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbFiltering.FormattingEnabled = True
        Me.cbFiltering.Items.AddRange(New Object() {"cannon", "epson", "hp"})
        Me.cbFiltering.Location = New System.Drawing.Point(283, 40)
        Me.cbFiltering.Name = "cbFiltering"
        Me.cbFiltering.Size = New System.Drawing.Size(145, 22)
        Me.cbFiltering.TabIndex = 27
        Me.ttt.SetToolTip(Me.cbFiltering, "Filtering Base on Operation System")
        '
        'ttt
        '
        Me.ttt.AutoPopDelay = 10000
        Me.ttt.InitialDelay = 50
        Me.ttt.IsBalloon = True
        Me.ttt.ReshowDelay = 100
        Me.ttt.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.ttt.ToolTipTitle = "Information"
        '
        'cbBlockqoute
        '
        Me.cbBlockqoute.AutoSize = True
        Me.cbBlockqoute.Checked = True
        Me.cbBlockqoute.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbBlockqoute.Location = New System.Drawing.Point(107, 44)
        Me.cbBlockqoute.Name = "cbBlockqoute"
        Me.cbBlockqoute.Size = New System.Drawing.Size(128, 18)
        Me.cbBlockqoute.TabIndex = 3
        Me.cbBlockqoute.Text = "Insert Blockqoute?"
        Me.ttt.SetToolTip(Me.cbBlockqoute, "Insert : Compatibility &amp; System Requirements blablabla ..." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10))
        Me.cbBlockqoute.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(518, 94)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(120, 22)
        Me.TextBox1.TabIndex = 29
        Me.TextBox1.Visible = False
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(518, 122)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(122, 22)
        Me.TextBox2.TabIndex = 30
        Me.TextBox2.Visible = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(520, 143)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(104, 25)
        Me.Button1.TabIndex = 31
        Me.Button1.Text = "hp debug test"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(87, 306)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(31, 14)
        Me.LinkLabel1.TabIndex = 32
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "CLog"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(185, 145)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 33
        Me.Button2.Text = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        Me.Button2.Visible = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(502, 327)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.LinkLabel1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.cbBlockqoute)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.cbFiltering)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbOnTop)
        Me.Controls.Add(Me.lblCSdk)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.lblCFirmware)
        Me.Controls.Add(Me.cbSelect)
        Me.Controls.Add(Me.lblCUtil)
        Me.Controls.Add(Me.rtbCustom)
        Me.Controls.Add(Me.lblCRecomd)
        Me.Controls.Add(Me.lblCSoftware)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.lblCDriver)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.pb1)
        Me.Controls.Add(Me.rtbFinalOutput)
        Me.Controls.Add(Me.lblModelCode)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnOpen)
        Me.Controls.Add(Me.btnCopy)
        Me.Controls.Add(Me.cbCustom)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.RichTextBox1)
        Me.Controls.Add(Me.btnGet)
        Me.Controls.Add(Me.tbUrl)
        Me.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Iddle .."
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents tbUrl As TextBox
    Friend WithEvents btnGet As Button
    Friend WithEvents RichTextBox1 As RichTextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cbCustom As CheckBox
    Friend WithEvents btnCopy As Button
    Friend WithEvents btnOpen As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents lblModelCode As Label
    Friend WithEvents rtbFinalOutput As RichTextBox
    Friend WithEvents pb1 As ProgressBar
    Friend WithEvents lblCRecomd As Label
    Friend WithEvents lblCDriver As Label
    Friend WithEvents lblCSoftware As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents lblCSdk As Label
    Friend WithEvents lblCFirmware As Label
    Friend WithEvents lblCUtil As Label
    Friend WithEvents Button4 As Button
    Friend WithEvents RichTextBox3 As RichTextBox
    Friend WithEvents RichTextBox4 As RichTextBox
    Friend WithEvents bg1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents rtbCustom As RichTextBox
    Private WithEvents cbSelect As ComboBox
    Friend WithEvents bg2 As System.ComponentModel.BackgroundWorker
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Button6 As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents Button7 As Button
    Friend WithEvents Timer1 As Timer
    Friend WithEvents Timer2 As Timer
    Friend WithEvents bg3 As System.ComponentModel.BackgroundWorker
    Friend WithEvents cbOnTop As CheckBox
    Friend WithEvents Label2 As Label
    Private WithEvents cbFiltering As ComboBox
    Friend WithEvents ttt As ToolTip
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents btnTest As Button
    Friend WithEvents txExpression As RichTextBox
    Friend WithEvents cbBlockqoute As CheckBox
    Friend WithEvents Button1 As Button
    Friend WithEvents LinkLabel1 As LinkLabel
    Friend WithEvents Button2 As Button
End Class
