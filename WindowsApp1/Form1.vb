﻿Imports System.Net
Imports System.IO
Imports System.Net.NetworkInformation
Imports System.Threading
Imports System.ComponentModel
Imports Newtonsoft.Json.Linq
Imports System.Runtime.InteropServices
Imports Newtonsoft.Json
Imports System.Text.RegularExpressions

Public Class Form1
    Dim kukis As New CookieContainer
    Dim baseCanon As String = "https://www.usa.canon.com/internet/PA_NWSupport/driversDownloads?model="
    Dim apiEpson As String = "https://madavaka.me/app/printGrab/epson.php?uri="
    Dim LINE As String = vbCrLf
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnGet.Click
        rtbFinalOutput.Text = ""

        If cbSelect.Text = "Canon" Then
            bg1.RunWorkerAsync()
        ElseIf cbSelect.Text = "epson" Then
            bg2.RunWorkerAsync()
        ElseIf cbSelect.Text = "hp" Then
            bg3.RunWorkerAsync()
        Else
            MsgBox("Please Select Provider ...")
        End If
    End Sub

    Public Sub SaveMyTemp()
        If rtbFinalOutput.Text = "" Then
            'nothing
        Else
            Dim appPath As String = Application.StartupPath()
            Dim loKasi As String = appPath + ""
            Dim filepath As String = IO.Path.Combine(loKasi, "temp.html")
            rtbFinalOutput.SaveFile(filepath, RichTextBoxStreamType.PlainText)
        End If
    End Sub
    Public Sub SaveMyCss()
        If rtbCustom.Text = "" Then

        Else
            Dim appPath As String = Application.StartupPath()
            Dim loKasi As String = appPath
            Dim filepath As String = IO.Path.Combine(loKasi, "custom.txt")
            rtbCustom.SaveFile(filepath, RichTextBoxStreamType.PlainText)
        End If
    End Sub
    Public Sub SaveMyExpression()
        If txExpression.Text = "" Then

        Else
            Dim appPath As String = Application.StartupPath()
            Dim loKasi As String = appPath
            Dim filepath As String = IO.Path.Combine(loKasi, "expression.md")
            txExpression.SaveFile(filepath, RichTextBoxStreamType.PlainText)
        End If
    End Sub
    Public Sub openInBrowser()
        Dim appPath As String = Application.StartupPath()
        Dim loKasi As String = appPath + ""
        Dim filepath As String = IO.Path.Combine(loKasi, "temp.html")
        Process.Start(filepath)
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CheckForIllegalCrossThreadCalls = False
        Me.Text = "Grabb3r G08loX" + " | ver." + My.Resources.ver
        cbSelect.SelectedIndex = 0

        Try
            Dim appPath As String = Application.StartupPath()
            Dim lokasi As String = appPath + "\custom.txt"
            rtbCustom.Text = System.IO.File.ReadAllText(lokasi)
        Catch ex As Exception

        End Try

        Try
            Dim appPath As String = Application.StartupPath()
            Dim lokasiEx As String = appPath + "\expression.md"
            txExpression.Text = System.IO.File.ReadAllText(lokasiEx)
        Catch ex As Exception

        End Try

        '---- auto str replace'
        Dim str As String = rtbCustom.Text
        TextBox1.Text = GetStr(str, "Install(.*)Driver by")
        'MsgBox(GetStr(str, "Install(.*)Driver by"))
        '---- end auto str replace'

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        pb1.Value = 10
        btnGet.Enabled = False
        Me.Text = "Getting Data ..." + " | ver." + My.Resources.ver
        ' keep use previous cookies for fast load (multiply request)~
        Dim rsl As String = GetHttp(tbUrl.Text, kukis)
        RichTextBox1.Text = rsl
        pb1.Value = 50

        '---------------------------------------------
        Dim thepage As String = RichTextBox1.Text
        pb1.Value = 100
    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As DoWorkEventArgs) Handles bg1.DoWork
        '------------------------------------
        '           ~ C A N O N ~
        '------------------------------------
        btnGet.Enabled = False
        pb1.Value = 10
        Me.Text = "Getting Data ..." + " | ver." + My.Resources.ver
        Dim rsl As String = GetHttp(tbUrl.Text, kukis)
        pb1.Value = 30
        'get Name Device
        Dim deviceName As String = GetStr(rsl, "<title>(.*)</title>")
        Me.Text = "Model - " + deviceName + " | ver." + My.Resources.ver

        'StringReplace
        TextBox2.Text = " " + deviceName + " "
        rtbCustom.Text = rtbCustom.Text.Replace(TextBox1.Text, TextBox2.Text)
        TextBox1.Text = TextBox2.Text

        'get Model Code
        lblModelCode.Text = GetStr(rsl, My.Resources.reg1 + "(.*)" + My.Resources.reg2)
        Dim prmCanon As String = "&os=" + cbFiltering.Text + "&type=DS&lang=English"
        Dim rslData As String = GetHttp(baseCanon + lblModelCode.Text + prmCanon, kukis)

        RichTextBox1.Text = rslData
        pb1.Value = 50
        Try
            'Deklarasi JSON FILE 
            ' -------------------------------------------------------------------------------
            Dim json As JObject = JObject.Parse(RichTextBox1.Text)
            Dim recommendedForYou As String = json.SelectToken("downloadFiles").SelectToken("recommendedForYou").SelectToken("pagination").SelectToken("totalItems")
            Dim drivers As String = json.SelectToken("downloadFiles").SelectToken("drivers").SelectToken("pagination").SelectToken("totalItems")
            Dim software As String = json.SelectToken("downloadFiles").SelectToken("software").SelectToken("pagination").SelectToken("totalItems")
            Dim utilities As String = json.SelectToken("downloadFiles").SelectToken("utilities").SelectToken("pagination").SelectToken("totalItems")
            Dim firmware As String = json.SelectToken("downloadFiles").SelectToken("firmware").SelectToken("pagination").SelectToken("totalItems")
            Dim sdk As String = json.SelectToken("downloadFiles").SelectToken("sdk").SelectToken("pagination").SelectToken("totalItems")

            ' -------------------------------------------------------------------------------
            lblCRecomd.Text = recommendedForYou
            lblCDriver.Text = drivers
            lblCSoftware.Text = software
            lblCUtil.Text = "Utilities = " + utilities
            lblCFirmware.Text = "Firmware = " + firmware
            lblCSdk.Text = "Sdk = " + sdk

            pb1.Value = 70

            ' -------------------------------------------------------------------------------
            ' WEB Structure
            rtbFinalOutput.Text += LINE + LINE + "<h4>Download Driver for - " + deviceName + "</h4>"
            If cbBlockqoute.Checked = True Then
                rtbFinalOutput.Text += My.Resources.blockquote
            End If

            'rtbFinalOutput.Text += LINE + LINE + My.Resources.author + LINE + LINE

            Dim fileName1 As String = json.SelectToken("downloadFiles").SelectToken("recommendedForYou").SelectToken("downloadFilesSubSet").SelectToken("downloadFiles").SelectToken("fileTitle")
            Dim fileSize1 As String = json.SelectToken("downloadFiles").SelectToken("recommendedForYou").SelectToken("downloadFilesSubSet").SelectToken("downloadFiles").SelectToken("fileSize")
            Dim fileUrl1 As String = json.SelectToken("downloadFiles").SelectToken("recommendedForYou").SelectToken("downloadFilesSubSet").SelectToken("downloadFiles").SelectToken("fileUrl")

            ' -------------------------------------------------------------------------------
            If recommendedForYou = 1 Then


                'Content
                rtbFinalOutput.Text += My.Resources.htmlStartCanon
                rtbFinalOutput.Text += My.Resources.idRecommended + "<tr>"
                rtbFinalOutput.Text += "<td><center>" + fileName1 + "</center></td>"
                rtbFinalOutput.Text += "<td><center>" + fileSize1 + "</center></td>"
                rtbFinalOutput.Text += "<td><center><button class='btn'><a href='" + fileUrl1 + "'>Download</a></button></center></td>"
                rtbFinalOutput.Text += "</tr></table>[/su_tab]"

            ElseIf recommendedForYou > 1 Then
                Dim str0 As String = GetStr(RichTextBox1.Text, My.Resources.reg7 + "(.*)" + My.Resources.reg8)
                Dim strR As String
                str0 = str0.Remove(0, 74)
                strR = "{" + str0
                Dim result = JsonConvert.DeserializeObject(strR)
                rtbFinalOutput.Text += My.Resources.htmlStartCanon
                rtbFinalOutput.Text += My.Resources.idRecommended
                For i As Integer = 0 To lblCRecomd.Text - 1
                    Try
                        rtbFinalOutput.Text += "<tr><td><center>" + result("downloadFiles")(i)("fileTitle") + "</center></td>"
                        rtbFinalOutput.Text += "<td><center>" + result("downloadFiles")(i)("fileSize") + "</center></td>"
                        rtbFinalOutput.Text += "<td><center><button class='btn'><a href='" + result("downloadFiles")(i)("fileUrl") + "'>Download</a></button></center></td>"
                        rtbFinalOutput.Text += "</tr>"

                    Catch ex As Exception
                    End Try
                Next
                rtbFinalOutput.Text += "</tr></table>[/su_tab]"
            Else
                rtbFinalOutput.Text += My.Resources.htmlStartCanon
            End If
            ' -------------------------------------------------------------------------------
            pb1.Value = 80


            'list all driver (optional) ->
            If drivers > 1 Then
                'RegexForDriver
                Dim str As String = GetStr(RichTextBox1.Text, My.Resources.reg3 + "(.*)" + My.Resources.reg4)
                str = str.Remove(0, 74)
                RichTextBox3.Text = "{" + str

                'Content
                rtbFinalOutput.Text += My.Resources.idDrivers
                Dim x = RichTextBox3.Text
                Dim result = JsonConvert.DeserializeObject(x)

                For i As Integer = 0 To lblCDriver.Text - 1
                    Try
                        rtbFinalOutput.Text += "<tr><td><center>" + result("downloadFiles")(i)("fileTitle") + "</center></td>"
                        rtbFinalOutput.Text += "<td><center>" + result("downloadFiles")(i)("fileSize") + "</center></td>"
                        rtbFinalOutput.Text += "<td><center><button class='btn'><a href='" + result("downloadFiles")(i)("fileUrl") + "'>Download</a></button></center></td>"
                        rtbFinalOutput.Text += "</tr>"
                    Catch ex As Exception
                        MsgBox(ex.ToString, vbInformation)
                    End Try
                Next
                'rtbFinalOutput.Text += "</tr></table></div></div>"
                rtbFinalOutput.Text += "</tr></table>[/su_tab]"

            ElseIf drivers = 1 And recommendedForYou > 0 Then
                rtbFinalOutput.Text += My.Resources.idDrivers + "<tr>"
                rtbFinalOutput.Text += "<td><center>" + fileName1 + "</center></td>"
                rtbFinalOutput.Text += "<td><center>" + fileSize1 + "</center></td>"
                rtbFinalOutput.Text += "<td><center><button class='btn'><a href='" + fileUrl1 + "'>Download</a></button></center></td>"
                rtbFinalOutput.Text += "</tr></table>[/su_tab]"
            Else
                rtbFinalOutput.Text += My.Resources.htmlStartCanon
            End If

            ' -------------------------------------------------------------------------------

            Try
                'list all software ->
                If software > 0 Then
                    Dim str2 As String = GetStr(RichTextBox1.Text, My.Resources.reg5 + "(.*)" + My.Resources.reg6)
                    str2 = str2.Remove(0, 74)
                    RichTextBox4.Text = "{" + str2

                    'Content
                    Dim x = RichTextBox4.Text
                    Dim result = JsonConvert.DeserializeObject(x)
                    rtbFinalOutput.Text += My.Resources.idSoftwares

                    For i As Integer = 0 To lblCSoftware.Text - 1
                        Try
                            rtbFinalOutput.Text += "<tr><td><center>" + result("downloadFiles")(i)("fileTitle") + "</center></td>"
                            rtbFinalOutput.Text += "<td><center>" + result("downloadFiles")(i)("fileSize") + "</center></td>"
                            rtbFinalOutput.Text += "<td><center><button class='btn'><a href='" + result("downloadFiles")(i)("fileUrl") + "'>Download</a></button></center></td>"
                            rtbFinalOutput.Text += "</tr>"
                        Catch ex As Exception
                            'MsgBox(ex.ToString, vbInformation)
                        End Try
                    Next
                    'rtbFinalOutput.Text += "</tr></table></div></div>"
                    rtbFinalOutput.Text += "</tr></table>[/su_tab]"

                ElseIf software > 9 Then
                    Dim str2 As String = GetStr(RichTextBox1.Text, My.Resources.reg5 + "(.*)" + My.Resources.reg6)
                    str2 = str2.Remove(0, 75)
                    RichTextBox4.Text = "{" + str2

                    'Content
                    Dim x = RichTextBox4.Text
                    Dim result = JsonConvert.DeserializeObject(x)
                    rtbFinalOutput.Text += My.Resources.idSoftwares

                    For i As Integer = 0 To lblCSoftware.Text - 1
                        Try
                            rtbFinalOutput.Text += "<tr><td><center>" + result("downloadFiles")(i)("fileTitle") + "</center></td>"
                            rtbFinalOutput.Text += "<td><center>" + result("downloadFiles")(i)("fileSize") + "</center></td>"
                            rtbFinalOutput.Text += "<td><center><button class='btn'><a href='" + result("downloadFiles")(i)("fileUrl") + "'>Download</a></button></center></td>"
                            rtbFinalOutput.Text += "</tr>"
                        Catch ex2 As Exception
                            'MsgBox(ex2.ToString, vbInformation)
                        End Try
                    Next
                    'rtbFinalOutput.Text += "</tr></table></div></div>"
                    rtbFinalOutput.Text += "</tr></table>[/su_tab]"

                Else
                    'rtbFinalOutput.Text += My.Resources.htmlStartCanon
                End If

            Catch ex As Exception

            End Try


            'End Template
            rtbFinalOutput.Text += My.Resources.htmlEndCanon

            If cbCustom.Checked = True Then
                rtbFinalOutput.Text += rtbCustom.Text
            End If

            'DONE
            pb1.Value = 100
        Catch ex As Exception
            'MsgBox(ex.ToString)
            pb1.Value = 100
        End Try
    End Sub

    Private Sub bg1_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bg1.RunWorkerCompleted
        btnGet.Enabled = True

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles btnOpen.Click
        Try
            SaveMyTemp()
            openInBrowser()
        Catch ex As Exception
            MsgBox("Nothing to Show ~")
        End Try

    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles cbCustom.CheckedChanged
        If cbCustom.Checked = False Then
            rtbCustom.Enabled = False
            txExpression.Enabled = False
            btnTest.Enabled = False
        Else
            rtbCustom.Enabled = True
            txExpression.Enabled = True
            btnTest.Enabled = True
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnCopy.Click
        Try
            If rtbFinalOutput.Text <> String.Empty Then
                Clipboard.SetText(rtbFinalOutput.Text)
                Timer1.Enabled = True
            Else
                Clipboard.Clear()
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Sub

    Private Sub bg2_DoWork(sender As Object, e As DoWorkEventArgs) Handles bg2.DoWork
        '------------------------------------
        '           ~ E P S O N ~
        '------------------------------------

        pb1.Value = 10
        btnGet.Enabled = False
        Me.Text = "Getting Data ..." + " | ver." + My.Resources.ver
        ' keep use previous cookies for fast load (multiply request)~

        Dim rsl As String = GetHttpX(apiEpson + tbUrl.Text) '+ "?review-filter=" + cbFiltering.Text)
        RichTextBox1.Text = rsl
        pb1.Value = 50



        '---------------------------------------------
        Dim thepage As String = RichTextBox1.Text
        'CREATE TEMP LIST ~
        Dim hasilData1 As New List(Of String)
        Dim hasilData2 As New List(Of String)
        Dim hasilData3 As New List(Of String)
        'SPLITE || aaaabbbbcccc -> abcabcabcabc ~
        Dim hasilPair As New List(Of String)

        '---- GET DATAS ----

        'datasTitle
        Dim r As New Regex("data-dl_title=(.*)""")
        Dim matches As MatchCollection = r.Matches(thepage)
        For Each element As Match In matches
            'Add to Temp1
            hasilData1.Add("<td>" + element.Value.Split("""").GetValue(1) + "</td>")
        Next
        pb1.Value = 70


        'datasType
        Dim pattern As String = "data-dl_type=""icc_(.*)"""
        Dim r2 As New Regex(pattern)
        Dim matches2 As MatchCollection = r2.Matches(thepage)
        For Each element2 As Match In matches2
            hasilData2.Add("<td><center>" + element2.Groups(1).Value.Substring(0, 1).ToUpper + element2.Groups(1).Value.Substring(1) + "</center></td>")
            'MsgBox(element2.Value.Split("""").GetValue(1))
            'MsgBox(element2.Groups(1).Value.Substring(0, 1).ToUpper + element2.Groups(1).Value.Substring(1))
        Next

        ' datasLinkDownload
        Dim r3 As New Regex("(href=""https://ftp.epson.com(.*)|href=""http://download.epson.com.sg(.*)|href=""http://download.ebz.epson.net(.*)|href=""http://support.epson.net/setupnavi)")
        Dim matches3 As MatchCollection = r3.Matches(thepage)
        For Each element3 As Match In matches3
            hasilData3.Add("<td><center><button class='btn'><a href='" + element3.Value.Split("""").GetValue(1) + "' target='_blank'>Download</a></button></center></td>")
        Next
        pb1.Value = 80

        'MsgBox(hasilData1.Count.ToString + " | " + hasilData2.Count.ToString + " | " + hasilData3.Count.ToString)
        pb1.Value = 90

        Try
            Dim getTitle As String = GetStr(RichTextBox1.Text, "<span class=""current"">(.*)</span>")
            rtbFinalOutput.Text += "<center><h3>Download Driver - " + getTitle + " for " + cbFiltering.Text + "</h3></center>"
            'rtbFinalOutput.Text += LINE + LINE + My.Resources.author + LINE + LINE


            If cbBlockqoute.Checked = True Then
                rtbFinalOutput.Text += My.Resources.blockquote
            End If

            Me.Text = "Model - " + getTitle + " | ver." + My.Resources.ver
            'StringReplace
            TextBox2.Text = " " + getTitle + " "
            rtbCustom.Text = rtbCustom.Text.Replace(TextBox1.Text, TextBox2.Text)
            TextBox1.Text = TextBox2.Text

            rtbFinalOutput.Text += My.Resources.htmlStartEpson

            Try
                For i As Integer = 0 To hasilData1.Count - 1
                    hasilPair.Add("<tr>" + hasilData1(i) + LINE + hasilData2(i) + LINE + hasilData3(i) + LINE + "</tr>")
                    rtbFinalOutput.Text += hasilPair.Item(i) + LINE
                Next

                rtbFinalOutput.Text += My.Resources.htmlEndEpson
                If cbCustom.Checked = True Then
                    rtbFinalOutput.Text += rtbCustom.Text
                End If
                pb1.Value = 100
            Catch ex As Exception
                rtbFinalOutput.Text += My.Resources.htmlEndEpson
                If cbCustom.Checked = True Then
                    rtbFinalOutput.Text += rtbCustom.Text
                End If
                pb1.Value = 100
            End Try




        Catch ex As Exception
            pb1.Value = 100
            'MsgBox(ex.ToString)

        End Try
    End Sub

    Private Sub bg2_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bg2.RunWorkerCompleted
        btnGet.Enabled = True

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbSelect.SelectedIndexChanged
        If cbSelect.Text = "Canon" Then
            cbFiltering.Enabled = True
            cbFiltering.Items.Clear()
            cbFiltering.Items.Add("WINDOWS_10_X64")
            cbFiltering.Items.Add("WINDOWS_10")
            cbFiltering.Items.Add("WINDOWS_2000")
            cbFiltering.Items.Add("WINDOWS_7")
            cbFiltering.Items.Add("WINDOWS_7_X64")
            cbFiltering.Items.Add("WINDOWS_8")
            cbFiltering.Items.Add("WINDOWS_8_X64")
            cbFiltering.Items.Add("WINDOWS_8_1")
            cbFiltering.Items.Add("WINDOWS_8_1_X64")
            cbFiltering.Items.Add("MACOS_10_14")
            cbFiltering.Items.Add("MACOS_10_13")
            cbFiltering.Items.Add("MACOS_V10_12")
            cbFiltering.Items.Add("OS_X_V10_11")
            cbFiltering.Items.Add("LINUX")

            cbFiltering.SelectedIndex = 0
        ElseIf cbSelect.Text = "epson" Then
            cbFiltering.Items.Clear()
            cbFiltering.Enabled = False
            cbFiltering.Items.Add("Windows 10 64-bit")
            cbFiltering.Items.Add("Windows 10 32-bit")
            cbFiltering.Items.Add("Windows 10 S")
            cbFiltering.Items.Add("Windows 8.1 32-bit")
            cbFiltering.Items.Add("Windows 8.1 64-bit")
            cbFiltering.Items.Add("Windows 8 32-bit")
            cbFiltering.Items.Add("Windows 8 64-bit")
            cbFiltering.Items.Add("Windows 7 32-bit")
            cbFiltering.Items.Add("Windows 7 64-bit")
            cbFiltering.Items.Add("Windows XP 32-bit")
            cbFiltering.Items.Add("Windows XP 64-bit")
            cbFiltering.Items.Add("Windows Vista 32-bit")
            cbFiltering.Items.Add("Windows Vista 64-bit")
            cbFiltering.Items.Add("Windows Server 2012")
            cbFiltering.Items.Add("Windows Server 2008 32-bit")
            cbFiltering.Items.Add("Windows Server 2008 64-bit")
            cbFiltering.Items.Add("Windows Server 2003 32-bit")
            cbFiltering.Items.Add("Windows Server 2003 64-bit")
            cbFiltering.Items.Add("macOS 10.14.x")
            cbFiltering.Items.Add("macOS 10.13.x")
            cbFiltering.Items.Add("macOS 10.12.x")
            cbFiltering.Items.Add("Mac OS X 10.11.x")
            cbFiltering.Items.Add("Mac OS X 10.10.x")
            cbFiltering.Items.Add("Mac OS X 10.9.x")
            cbFiltering.Items.Add("Mac OS X 10.8.x")
            cbFiltering.Items.Add("Mac OS X 10.7.x")
            cbFiltering.Items.Add("Mac OS X 10.6.x")
            cbFiltering.Items.Add("Mac OS X 10.5.x")
            cbFiltering.Items.Add("Linux")
            cbFiltering.SelectedIndex = 0
        ElseIf cbSelect.Text = "hp" Then
            cbFiltering.Enabled = False
        End If
    End Sub

    Private Sub Button5_Click_1(sender As Object, e As EventArgs) Handles Button5.Click
        Form2.RichTextBox1.Text = RichTextBox1.Text
        Form2.Text = "Result"
        Form2.ShowDialog()
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Form2.Text = "Edit CSS"
        Form2.ShowDialog()
    End Sub

    Private Sub RichTextBox5_TextChanged(sender As Object, e As EventArgs) Handles rtbCustom.TextChanged
        SaveMyCss()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        rtbFinalOutput.BackColor = Color.MediumAquamarine
        Timer1.Enabled = False
        Timer2.Enabled = True
    End Sub

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        rtbFinalOutput.BackColor = Color.FromArgb(244, 244, 244)
        Timer2.Enabled = False

    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        Form3.ShowDialog()
    End Sub

    Private Sub bg3_DoWork(sender As Object, e As DoWorkEventArgs) Handles bg3.DoWork
        '--------------------------------------------------------
        '           H P  |  S U P P O R T
        '--------------------------------------------------------

        Dim temp1 As String = GetHttp("https://support.hp.com/us-en/drivers/selfservice/hp-laserjet-1020-printer-series/439423?jumpid=in_r11839_us/en/SWDLanding/TopProducts", kukis)
        If temp1.Contains("<base href=") Then
            Dim getBase As String = GetStr(temp1, "<base href=""(.*)"">")
            Dim getJson As String = "/dl5/d5/L2dBISEvZ0FBIS9nQSEh/pw/Z7_M0I02JG0KGVO00AUBO4GT60082/res/id=getSoftwareDriverDetails/c=cacheLevelPage/=/"
            Dim temp2 As String = PostHttp(kukis, getBase + getJson, "requestJson=%7B%22productNameOid%22%3A%22439423%22%2C%22urlLanguage%22%3A%22en%22%2C%22language%22%3A%22en%22%2C%22osId%22%3A%22792898937266030878164166465223921%22%2C%22countryCode%22%3A%22us%22%2C%22detectedOSBit%22%3A%22%22%2C%22platformName%22%3A%22Windows%22%2C%22platformId%22%3A%22487192269364721453674728010296573%22%2C%22versionName%22%3A%22Windows+10+(64-bit)%22%2C%22versionId%22%3A%22792898937266030878164166465223921%22%2C%22osLanguageName%22%3A%22en%22%2C%22osLanguageCode%22%3A%22en%22%2C%22hpTermsOfUseURL%22%3A%22https%3A%2F%2Fsupport.hp.com%2Fus-en%2Fdocument%2Fc00581401%22%2C%22inOSDriverLinkURL%22%3A%22https%3A%2F%2Fsupport.hp.com%2Fus-en%2Fdocument%2Fc01796879%22%2C%22languageValue%22%3A%22English%22%2C%22termsOfUseTitle%22%3A%22By+downloading+you+agree+to+HP's+Terms+of+Use+undefined%22%2C%22privyPolicyURL%22%3A%22http%3A%2F%2Fwww8.hp.com%2Fus%2Fen%2Fprivacy%2Fprivacy.html%22%2C%22bitInfoUrl%22%3A%22https%3A%2F%2Fsupport.hp.com%2Fus-en%2Fdocument%2Fc03666764%22%2C%22sku%22%3A%22%22%2C%22productSeriesOid%22%3A%22439423%22%2C%22productSeriesName%22%3A%22hp-laserjet-1020-printer-series%22%7D")
            Dim fixtoAscii As String = UnicodeToAscii(temp2)


            Dim thepage As String = fixtoAscii

            MsgBox(thepage)
            'CREATE TEMP LIST ~
            Dim hasilData1 As New List(Of String)
            'SPLITE || aaaabbbbcccc -> abcabcabcabc ~
            Dim hasilPair As New List(Of String)
            '---- GET DATAS ----
            'datasTitle
            Dim r As New Regex("\title""(.*)"",""")
            Dim matches As MatchCollection = r.Matches(thepage)
            For Each element As Match In matches
                'Add to Temp1
                'hasilData1.Add("<td>" + element.Value.Split(""":""").GetValue(1) + "</td>")
                hasilData1.Add("######" + element.Value.Split(":").GetValue(1) + "######")
            Next

            For i As Integer = 0 To hasilData1.Count - 1
                hasilPair.Add(hasilData1(i) + LINE)
                MsgBox(hasilPair.Item(i))

            Next

        End If

    End Sub

    Private Sub CheckBox2_CheckedChanged(sender As Object, e As EventArgs) Handles cbOnTop.CheckedChanged
        If cbOnTop.Checked = True Then
            Me.TopMost = True
        Else
            Me.TopMost = False
        End If
    End Sub

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        End
    End Sub

    Private Sub Form1_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        End
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button1_Click_2(sender As Object, e As EventArgs) Handles btnTest.Click
        '---- auto str replace'
        Dim str As String = rtbCustom.Text
        TextBox1.Text = GetStr(str, txExpression.Text)
        MsgBox(GetStr(str, txExpression.Text))
        '---- end auto str replace'
    End Sub

    Private Sub txExpression_TextChanged(sender As Object, e As EventArgs) Handles txExpression.TextChanged
        SaveMyExpression()
    End Sub

    Private Sub Button1_Click_3(sender As Object, e As EventArgs) Handles Button1.Click
        bg3.RunWorkerAsync()
        Button1.Enabled = False
    End Sub

    Private Sub bg3_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bg3.RunWorkerCompleted
        Button1.Enabled = True

    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs)

    End Sub


    Private Sub Button2_Click_2(sender As Object, e As EventArgs)

    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        MsgBox(My.Resources.clog, vbInformation)
    End Sub

    Private Sub cbFiltering_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbFiltering.SelectedIndexChanged

    End Sub

    Private Sub Button2_Click_3(sender As Object, e As EventArgs) Handles Button2.Click
        Dim rsl As String = GetHttpX(tbUrl.Text + "?review-filter=" + cbFiltering.Text)
        MsgBox(rsl)
    End Sub
End Class
