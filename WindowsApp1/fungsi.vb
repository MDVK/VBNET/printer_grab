﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Text.RegularExpressions

Module fungsi
    Private Const USER_AGENT As String = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36"

    Public Function GetHttp(ByVal uri As String, ByVal lpCookie As CookieContainer) As String
        Try
            Dim reader As StreamReader
            Dim Request As HttpWebRequest = HttpWebRequest.Create(uri)
            Request.UserAgent = USER_AGENT
            Request.AllowAutoRedirect = False
            Request.Timeout = 15000
            Request.ReadWriteTimeout = 15000
            Request.CookieContainer = lpCookie

            Dim Response As HttpWebResponse = Request.GetResponse()

            reader = New StreamReader(Response.GetResponseStream())
            Dim responseStr As String = reader.ReadToEnd()
            Response.Close()
            reader.Close()
            Return responseStr
        Catch ex As Exception
            'MsgBox("Request Time Out .. ", vbExclamation)
            MsgBox(ex.ToString)
        End Try
        Return ""
    End Function
    Public Function GetHttpX(ByVal uri As String) As String

        Dim reader As StreamReader
            Dim Request As HttpWebRequest = HttpWebRequest.Create(uri)
            Request.UserAgent = USER_AGENT
            Request.AllowAutoRedirect = False
            Request.Timeout = 15000
            Request.ReadWriteTimeout = 15000
            Dim Response As HttpWebResponse = Request.GetResponse()
            reader = New StreamReader(Response.GetResponseStream())
            Dim responseStr As String = reader.ReadToEnd()
            Response.Close()
            reader.Close()
            Return responseStr
        Return ""
    End Function

    Public Function GetStr(lpBaseString As String, lpPattern As String) As String
        Try
            Dim ketemu As Match = Regex.Match(lpBaseString, lpPattern)

            If ketemu.Success Then
                Dim result As String = ketemu.Groups(1).Value
                Return result
            Else
                Return False
            End If
        Catch ex As Exception

        End Try
        Return ""
    End Function


    Public Function PostHttp(ByRef lpCookie As CookieContainer, ByVal lpURL As String, ByVal lpPostData As String) As String
        Try
            Dim reader As StreamReader
            Dim Request As HttpWebRequest = HttpWebRequest.Create(lpURL)
            Request.UserAgent = USER_AGENT
            Request.CookieContainer = lpCookie
            Request.Timeout = 15000
            Request.ReadWriteTimeout = 15000
            Request.AllowAutoRedirect = False
            Request.ContentType = "application/x-www-form-urlencoded"
            Request.Method = "POST"
            Request.ContentLength = lpPostData.Length
            Dim requestStream As Stream = Request.GetRequestStream()
            Dim postBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(lpPostData)
            requestStream.Write(postBytes, 0, postBytes.Length)
            requestStream.Close()
            Dim Response As HttpWebResponse = Request.GetResponse()
            For Each tempCookie In Response.Cookies
                lpCookie.Add(tempCookie)
            Next
            reader = New StreamReader(Response.GetResponseStream())
            Dim str As String = reader.ReadToEnd()
            Return str
            Response.Close()
            reader.Close()

        Catch ex As Exception

        End Try

        Return ""
    End Function

    Function stringBuilderr(minCharacters As Integer, maxCharacters As Integer, expresion As String)
        Static r As New Random
        Dim chactersInString As Integer = r.Next(minCharacters, maxCharacters)
        Dim sb As New StringBuilder
        For i As Integer = 1 To chactersInString
            Dim idx As Integer = r.Next(0, expresion.Length)
            sb.Append(expresion.Substring(idx, 1))
        Next
        Return sb.ToString()
    End Function


    Public Function UnicodeToAscii(ByVal valuE As String) As String
        Try
            Dim input As String = valuE

            Dim fix1 As String = valuE.Replace(My.Resources.fixHp1, """:""")
            Dim fix2 As String = fix1.Replace(My.Resources.fixHp2, """,""")
            Dim fix3 As String = fix2.Replace(My.Resources.fixHp3, "{""")
            Dim fix4 As String = fix3.Replace(My.Resources.fixHp4, """:{""")
            Dim fix5 As String = fix4.Replace(My.Resources.fixHp5, """:[{""")
            Dim fix6 As String = fix5.Replace(My.Resources.fixHp6, """")

            valuE = fix6

            Return valuE

        Catch ex As Exception

        End Try
        Return ""
    End Function

End Module
