﻿Imports System.ComponentModel
Imports System.Net

Public Class Form3
    Dim kukis As New CookieContainer

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        bg1.RunWorkerAsync()
        Me.Text = "Please Wait ..."
    End Sub

    Private Sub Form3_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = Form1.Icon
        CheckForIllegalCrossThreadCalls = False
        cbSelect.SelectedIndex = 0
    End Sub

    Private Sub bg1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bg1.DoWork
        Button1.Enabled = False
        rtbInput.Enabled = False
        rtbOutput.Enabled = False
        cbSelect.Enabled = False

        Dim rsl As String = GetHttp("https://madavaka.me/app/Spinner/index.php?" +
                                    "s=" + rtbInput.Text +
                                    "&q=" + cbSelect.Text,
                                    kukis)
        rtbOutput.Text = rsl
    End Sub

    Private Sub bg1_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bg1.RunWorkerCompleted
        Button1.Enabled = True
        rtbInput.Enabled = True
        rtbOutput.Enabled = True
        cbSelect.Enabled = True
        Me.Text = "WordAI Spinner Client"
    End Sub
End Class