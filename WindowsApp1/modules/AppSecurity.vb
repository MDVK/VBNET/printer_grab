﻿Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Module AppSecurity
    Public DES As New TripleDESCryptoServiceProvider
    Public MD5 As New MD5CryptoServiceProvider
    Public ZxcBnm As String = My.Resources.ADIT_GANTENG 'TRUST ME -> NO COMMENT | NO EDIT. THIS LINE 


    Public Function MD5Hash(ByVal value As String) As Byte()
        Return MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(value))
    End Function

    Public Function EnCokAbanG(ByVal stringToEncrypt As String, ByVal key As String) As String
        DES.Key = MD5Hash(key)
        DES.Mode = CipherMode.ECB
        Dim Buffer As Byte() = ASCIIEncoding.ASCII.GetBytes(stringToEncrypt)
        Return Convert.ToBase64String(DES.CreateEncryptor().TransformFinalBlock(Buffer, 0, Buffer.Length))
    End Function

    Public Function DeDeKimCiL(ByVal encryptedString As String, ByVal key As String) As String
        'Try
        DES.Key = MD5Hash(key)
        DES.Mode = CipherMode.ECB
        Dim Buffer As Byte() = Convert.FromBase64String(encryptedString)
        Return ASCIIEncoding.ASCII.GetString(DES.CreateDecryptor().TransformFinalBlock(Buffer, 0, Buffer.Length))
        'Catch ex As Exception
        ' MessageBox.Show("Invalid", "Decryption Failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        'End Try
    End Function
End Module
