﻿Option Explicit On

Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Web

Module Module1
    Dim drag As Boolean
    Dim mousex As Integer
    Dim mousey As Integer

    Public kEy As String
    Public basE As String
    Public Const LINE As String = vbCrLf
    Private Const USER_AGENT As String = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36"
    Private Declare Ansi Function GetPrivateProfileString Lib "kernel32.dll" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    Private Declare Ansi Function WritePrivateProfileString Lib "kernel32.dll" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Integer

    'Function to read from INI File
    Public Function GetIniSetting(ByVal strINIFile As String, ByVal strSection As String, ByVal strKey As String) As String
        Dim strValue As String = ""
        Try
            strValue = Space(1024)
            GetPrivateProfileString(strSection, strKey, "NOT_FOUND", strValue, 1024, strINIFile)
            Do While InStrRev(strValue, " ") = Len(strValue)
                strValue = Mid(strValue, 1, Len(strValue))
            Loop
            ' to remove a special chr in the last place
            strValue = Mid(strValue, 1, Len(strValue))
            GetIniSetting = strValue

        Catch ex As Exception
            If Err.Number <> 0 Then Err.Raise(Err.Number, , "Error form Functions.SetIniSettings " & Err.Description)
        End Try
        Return strValue
    End Function
    'Prosedure to write INI file
    Public Sub SetIniSettings(ByVal strINIFile As String, ByVal strSection As String, ByVal strKey As String, ByVal strValue As String)
        Try '
            WritePrivateProfileString(strSection, strKey, strValue, strINIFile)
        Catch ex As Exception
            If Err.Number <> 0 Then Err.Raise(Err.Number, , "Error form Functions.SetIniSettings " & Err.Description)
        End Try
    End Sub


    Public Sub simPaN(ByVal kEy As String)
        Call SaveSetting("aditSoft", "config", "grabber", kEy)
        Call ambiL()
    End Sub
    Public Sub ambiL()
        kEy = GetSetting("aditSoft", "config", "grabber", "")
    End Sub

    Public Sub simPaN_basE(ByVal basE As String)
        Call SaveSetting("aditSoft", "config", "server", basE)
        Call ambiL()
    End Sub
    Public Sub ambiL_basE()
        basE = GetSetting("aditSoft", "config", "server", "")
    End Sub
    '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@



End Module